package com.decamp.rainbowrequestinputrestapi.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.decamp.rainbowrequestinputrestapi.service.RainbowService;

@RestController
@RequestMapping("/")
@CrossOrigin
public class RainbowController {
    @Autowired
    private RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> filterQuerryString(@RequestParam String querryString) {
        return rainbowService.filterQuerry(querryString);

    }

    @GetMapping("/rainbow-request-param/{index}")
    public String filterIndexParam(@PathVariable(value = "index") int index) {
        return rainbowService.filterIndex(index);

    }

}
