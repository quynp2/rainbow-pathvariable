package com.decamp.rainbowrequestinputrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowRequestInputRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowRequestInputRestApiApplication.class, args);
	}

}
