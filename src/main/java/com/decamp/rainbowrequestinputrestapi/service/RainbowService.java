package com.decamp.rainbowrequestinputrestapi.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    private String[] Rainbow = new String[] { "red", "orange", "yellow", "green", "blue", "indigo", "violet" };

    public ArrayList<String> filterQuerry(String querryString) {
        ArrayList<String> result = new ArrayList<>();
        for (String color : Rainbow) {
            if (color.contains(querryString)) {
                result.add(color);
            }

        }
        return result;

    }

    public String filterIndex(int IndexString) {
        String result = new String();
        if (IndexString >= 0 || IndexString <= 6) {
            result = this.Rainbow[IndexString];
        }
        return result;

    }

}
